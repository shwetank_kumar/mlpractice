import pandas as pd
import quandl
import math

quandl.ApiConfig.api_key = '4tArX7trhZsKdBRNrTMx'
df = quandl.get_table('WIKI/PRICES')

df = df[['adj_open', 'adj_high', 'adj_low', 'adj_close', 'adj_volume',]]
df['HL_PCT'] = (df['adj_high'] - df['adj_low']) / df['adj_low'] * 100.0
df['PCT_change'] = (df['adj_close'] - df['adj_open']) / df['adj_open'] * 100.0

df = df[['adj_close', 'HL_PCT', 'PCT_change', 'adj_volume']]

forecast_col = 'adj_close'
df.fillna(-99999, inplace=True)

forecast_out = int(math.ceil(0.01*len(df)))

df['label'] = df[forecast_col].shift(-forecast_out)
df.dropna(inplace=True)
print(df.head())